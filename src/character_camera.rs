use bevy::{prelude::*, render::camera::ScalingMode};

pub struct CharacterCameraPlugin;

impl Plugin for CharacterCameraPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<CameraMoved>()
            .add_systems(Startup, spawn_camera)
            .add_systems(PostUpdate, move_camera)
			;
    }
}

#[derive(Component)]
pub struct CharacterCamera;

#[derive(Event, Debug, Default)]
pub struct CameraMoved(pub Vec3);

#[derive(Component, Debug, Default)]
pub struct CameraTarget(pub Vec3);

pub fn spawn_camera(mut commands: Commands) {
    commands
        .spawn(Camera2dBundle {
            projection: OrthographicProjection {
                far: 1000.,
                near: -1000.,
                scaling_mode: ScalingMode::FixedHorizontal(3840.0),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(CharacterCamera);
}

pub fn move_camera(
    mut camera_query: Query<(&mut Transform, With<CharacterCamera>)>,
    target_query: Query<&CameraTarget>,
    mut camera_moved_event: EventWriter<CameraMoved>,
) {
    let (mut camera_pos, ..) = camera_query.get_single_mut().unwrap();
    let Ok(target) = target_query.get_single() else { return };
    let d = Vec3 {
        x: target.0.x - camera_pos.translation.x,
        y: target.0.y - camera_pos.translation.y,
        z: 0.0,
    };
    camera_pos.translation += d;
    camera_moved_event.send(CameraMoved(d));
}

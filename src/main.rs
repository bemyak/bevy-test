mod character_camera;
mod parallax;
mod player;

use bevy::{
    prelude::*,
    render::{
        settings::{PowerPreference, WgpuSettings},
        RenderPlugin,
    },
    window::WindowResolution,
};
use character_camera::{CameraTarget, CharacterCamera, CharacterCameraPlugin};
use parallax::{Parallax, ParallaxPlugin};
use player::{move_player, Player};

fn main() {
    App::new()
        // Bevy plugins
        .add_plugins(
            DefaultPlugins
                .set(
                    // Use integrated graphics card to workaround NVidia issue
                    RenderPlugin {
                        wgpu_settings: WgpuSettings {
                            power_preference: PowerPreference::LowPower,
                            ..default()
                        },
                    },
                )
                // Set default window ratio to 4:3
                .set(WindowPlugin {
                    primary_window: Some(Window {
                        resolution: WindowResolution::new(1280.0, 960.0),
                        ..Default::default()
                    }),
                    ..Default::default()
                }),
        )
        .add_plugins(CharacterCameraPlugin)
        .add_plugins(ParallaxPlugin)
        .add_systems(Startup, spawn)
        .add_systems(Update, move_player)
        .add_systems(Last, verify)
        .run();
}

fn spawn(mut commands: Commands, assets: Res<AssetServer>) {
    commands
        .spawn(SpriteBundle {
            texture: assets.load("bg.png"),
            transform: Transform::from_xyz(0.0, 0.0, 0.0),
            ..Default::default()
        })
        .insert(Parallax { scale: 0.0 })
        .insert(Verify);

    let bush = assets.load("bush.png");
    for x in 1..50 {
        spawn_bush(&mut commands, bush.clone(), x as f32);
    }

    commands
        .spawn(SpriteBundle {
            texture: assets.load("idle.png"),
            transform: Transform::from_xyz(0.0, 0.0, 100.0),
            ..Default::default()
        })
        .insert(CameraTarget::default())
        .insert(Player);
}

fn spawn_bush(commands: &mut Commands, image: Handle<Image>, x: f32) {
    commands
        .spawn(SpriteBundle {
            texture: image,
            transform: Transform::from_xyz(0.0, 0.0, x),
            ..Default::default()
        })
        .insert(Parallax { scale: x * 0.1 });
}

#[derive(Component)]
struct Verify;

fn verify(
    sprite_q: Query<(&Transform, With<Verify>)>,
    camera_q: Query<(&Transform, With<CharacterCamera>)>,
) {
    let (sprite, ..) = sprite_q.get_single().unwrap();
    let (camera, ..) = camera_q.get_single().unwrap();
    assert_eq!(sprite.translation.x, camera.translation.x);
    assert_eq!(sprite.translation.y, camera.translation.y);
    info!("ok");
}

use bevy::prelude::*;

use crate::character_camera::{move_camera, CameraMoved, CameraTarget, CharacterCamera};

pub struct ParallaxPlugin;

impl Plugin for ParallaxPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(PostUpdate, move_parallax.after(move_camera));
    }
}

#[derive(Component, Default, Debug)]
pub struct Parallax {
    pub scale: f32,
}

fn move_parallax(
    mut layer_query: Query<(
        &mut Transform,
        &Parallax,
        Without<CameraTarget>,
        Without<CharacterCamera>,
    )>,
    mut camera_moved_event: EventReader<CameraMoved>,
) {
    let d: Vec3 = camera_moved_event.iter().map(|e| e.0).sum();
    for (mut transform, parallax, ..) in layer_query.iter_mut() {
        transform.translation += d * (1.0 - parallax.scale);
    }
}
